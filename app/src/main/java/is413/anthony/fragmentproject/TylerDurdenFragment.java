package is413.anthony.fragmentproject;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Anthony on 3/24/2015.
 */
public class TylerDurdenFragment extends Fragment
{
    private VideoView videoView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tyler_durden, container, false);

        videoView = (VideoView) v.findViewById(R.id.videoView);

        playVid();

        return v;
    }

    private void playVid()
    {Uri video = Uri.parse("android.resource://" + MainActivity.PACKAGE_NAME + "/"
            + R.raw.fightclub);

        videoView.setVideoURI(video);

        videoView.start();
    }


}
