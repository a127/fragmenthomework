package is413.anthony.fragmentproject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity
{
    private TylerDurdenFragment     tdf;
    private AndyDusfreneFragment    adf;
    private MichaelCorleoneFragment mcf;
    private FragmentManager frag_man;

    private RadioButton radio;

    public static String PACKAGE_NAME;

    //@InjectView(R.id.radioButton)   RadioButton rb1;
    //@InjectView(R.id.radioButton2)  RadioButton rb2;
    //@InjectView(R.id.radioButton3)  RadioButton rb3;


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.brad_pitt:
                FragmentTransaction tyler_ft = frag_man.beginTransaction();
                tyler_ft.replace(R.id.fragment_container, tdf);
                tyler_ft.commit();
                break;

            case R.id.tim_robbins:
                FragmentTransaction andy_ft = frag_man.beginTransaction();
                andy_ft.replace(R.id.fragment_container, adf);
                andy_ft.commit();
                break;

            case R.id.al_pacino:
                FragmentTransaction michael_ft = frag_man.beginTransaction();
                michael_ft.replace(R.id.fragment_container, mcf);
                michael_ft.commit();
                break;
        }

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PACKAGE_NAME = getApplicationContext().getPackageName();

        ButterKnife.inject(this);

//        rb1.setOnClickListener(new MyClickListener());
//        rb2.setOnClickListener(new MyClickListener());
//        rb3.setOnClickListener(new MyClickListener());

        tdf = new TylerDurdenFragment();
        adf = new AndyDusfreneFragment();
        mcf = new MichaelCorleoneFragment();

        frag_man = getFragmentManager();

    }

    private class MyClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            switch(v.getId())
            {
                //case R.id.radioButton:
                //case R.id.radioButton2:
                //case R.id.radioButton3:


            }

        }
    }
}
