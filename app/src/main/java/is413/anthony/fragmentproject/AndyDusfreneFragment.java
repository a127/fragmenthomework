package is413.anthony.fragmentproject;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import java.util.zip.Inflater;

/**
 * Created by Anthony on 3/24/2015.
 */
public class AndyDusfreneFragment extends Fragment
{
    private VideoView videoView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle)
    {
        super.onCreateView(inflater, container, bundle);

        View v = inflater.inflate(R.layout.fragment_andy_dufresne, container, false);

        videoView = (VideoView)v.findViewById(R.id.videoView2);

        playVid();

        return v;

    }

    private void playVid()
    {Uri video = Uri.parse("android.resource://" + MainActivity.PACKAGE_NAME + "/"
            + R.raw.getbusy);

        videoView.setVideoURI(video);

        videoView.start();
    }

}
