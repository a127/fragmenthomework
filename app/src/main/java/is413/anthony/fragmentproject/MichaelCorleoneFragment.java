package is413.anthony.fragmentproject;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

/**
 * Created by Anthony on 3/24/2015.
 */
public class MichaelCorleoneFragment extends Fragment
{
    private VideoView videoView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b)
    {
        super.onCreateView(inflater, container, b);

        View v = inflater.inflate(R.layout.fragment_michael_corleone, container, false);

        videoView = (VideoView)v.findViewById(R.id.videoView3);

        playVid();

        return v;
    }

    private void playVid()
    {Uri video = Uri.parse("android.resource://" + MainActivity.PACKAGE_NAME + "/"
            + R.raw.donttakesides);

        videoView.setVideoURI(video);

        videoView.start();
    }


}
